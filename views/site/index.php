<?php
require_once(ROOT . '/views/layouts/header.php');
?>
    <main>
        <div class="button">
            <span class="add_film"><a href="add_film">Добавить фильм</a></span>
            <form action="search" method="post">
                <input type="search" name="search" placeholder="поиск">
            </form>
        </div>
        <div class="film">
            <span class="import"><a href="import">Импортировать даные</a></span>
            <span class="create"><a href="create">Создать таблицу</a></span>
            <form enctype="multipart/form-data" action="load" method="post">
                <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
                <input type="file" name="file" class="button_file"><br>
                <input type="submit" name="load" value="Загрузить" class="button_load">
            </form>
            <ol>
                <?php foreach ($listFilms as $list) :?>
                <a href="/show/<?php echo $list['id'];?>"><li><?php echo $list['title'];?></li></a>
                <?php endforeach;?>
            </ol>
        </div>

    </main>
<?php require_once (ROOT . '/views/layouts/footer.php');
