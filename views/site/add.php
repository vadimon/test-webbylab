<?php
require_once(ROOT . '/views/layouts/header.php');
?>
<section>
    <div class="add_form">
        <form  action="" method="post" >
            <input name="title" type="text" placeholder="Название" required><br>
            <input name="year" type="text" placeholder="Год випуска" required><br>
            <input name="format" type="text" placeholder="Формат" required><br>
            <p><textarea name="stars" placeholder="Список актеров" required></textarea></p>
            <input name="submit" type="submit" value="Добавить">
        </form>
    </div>
</section>
<?php require_once(ROOT . '/views/layouts/footer.php');
