<?php
require_once(ROOT . '/views/layouts/header.php');
?>
<section>
    <h2 style="color:#CC0000"><?php echo $showFilm['title'];?></h2>
    <b>Формат:</b><span style="color:#CC0000"><?php echo $showFilm['format'];?></span>
    <br><b>Год випуска:</b><span style="color:#CC0000"><?php echo $showFilm['year'];?></span>
    <br><b>Актеры:</b>
    <span style="color:#44a1c7"><?php echo $showFilm['stars'];?></span><br>
    <a href="../delete/<?=$showFilm['id']?>" class="button_delete">Удалить</a>
</section>
<?php require_once(ROOT . '/views/layouts/footer.php');