<?php


class Films
{

    //добавление фильма в БД
    public static function AddFilm($title, $year,$format,$stars)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO films (title, year, format, stars)
                        VALUE (:title, :year, :format, :stars)';
        $statement = $db->prepare($sql);
        $statement->bindParam(':title', $title);
        $statement->bindParam(':year', $year);
        $statement->bindParam(':format', $format);
        $statement->bindParam(':stars', $stars);
        return $statement->execute();
    }

    //показывает информацыю о фильме по индетифекатору id- $id
    public static function ShowFilm($id = false)
    {
        if ($id){
            $db = Db::getConnection();
            $sql = "SELECT * FROM films WHERE id = $id";
            $film = $db->prepare($sql);
            $film->execute();
            return $film->fetch(PDO::FETCH_ASSOC);
        }

    }

    //удаление фильма из БД
    public static function DeleteFilm($id = false)
    {
        if ($id){
            $db = Db::getConnection();
            $sql = "DELETE FROM films WHERE id = $id";
            $del = $db->prepare($sql);
            return $del->execute();
        }
    }

    //Вивод списка фильмов
    public static function ListFilm()
    {
        $db = Db::getConnection();
        $sql = 'SELECT id, title FROM films WHERE id > 0 ORDER BY title LIMIT 10';
        $listFilm =$db->prepare($sql);
        $listFilm->execute();
        return $listFilm->fetchAll(PDO::FETCH_ASSOC);
    }

    //Поиск
    public static function SearchFilm($search = false)
    {
        $db = Db::getConnection();
        $sql = "SELECT * FROM films WHERE title LIKE :search";
        $result = $db->prepare($sql);
        $result->bindParam(":search", $search);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }

    //Запрос на импорт даних из файла в БД



}