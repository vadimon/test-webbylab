<?php

require_once (ROOT.'/models/Films.php');
class SiteController
{

    // создание таблицы в БД
    public function actionCreateTable()
    {
        try {
            $table = 'films';
            $db = Db::getConnection();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "CREATE table $table(
     id INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
     title VARCHAR( 250 ) NOT NULL, 
     year INT ( 11 ) NOT NULL,
     format VARCHAR( 150 ) NOT NULL, 
     stars VARCHAR( 150 ) NOT NULL)";
            $db->exec($sql);
            echo "Таблица $table создана успешно!";
        }
        catch (PDOException $e)
        {
            echo $sql . "<br>" . $e->getMessage();
        }
        return true;
    }


    // загрузка главной страницы
    public function actionIndex()
    {
        $listFilms = Films::ListFilm();
        require_once(ROOT . '/views/site/index.php');
        return true;
    }

    //показывает информацыю о фильме
    public function actionShow($id = null)
    {
        if ($id) {
            $showFilm = Films::ShowFilm($id);
            require_once(ROOT . '/views/site/show.php');
            return true;
        }
    }

    //добавление фильма в БД
    public function actionAdd()
    {
        $title = '';
        $year = '';
        $format = '';
        $stars = '';
        $result = false;
        if (!empty($_POST)) {
            $title = $_POST['title'];
            $year = $_POST['year'];
            $format = $_POST['format'];
            $stars = $_POST['stars'];
            $result = Films::AddFilm($title, $year, $format, $stars);
        }
        require_once(ROOT . '/views/site/add.php');
        return true;
    }


    //удаление фильма из БД
    public function actionDelete($id = null)
    {
        $delete = Films::DeleteFilm($id);
        require_once (ROOT . '/views/site/delete.php');
        return true;
    }

    //Поиск
    public function actionSearch()
    {
        if (!empty($_POST)){
            $search = $_POST['search'];
            $resultSearch = Films::SearchFilm($search);
            require_once (ROOT . '/views/site/search.php');
        }

        return true;
    }

    // загрузка файла
    public function actionLoad()
    {
        $uploaddir = (ROOT . '/templete/uploads/');
        $uploadfile = $uploaddir . basename($_FILES['file']['name']);
        echo '<pre>';
        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
            echo "Файл корректен и был успешно загружен.\n";
        } else {
            echo "Возможная атака с помощью файловой загрузки!\n";
        }
        header('refresh: 2; url=index');
        return true;
    }

    public function actionImport()
    {
        $db = Db::getConnection();

        $file = file_get_contents('e:\\wamp\\www\\test1\\templete\\uploads\\sample_movies.txt');
        $file = explode("\n\r", $file);
        $stringForBD = '';
        foreach ($file as $value){
            $str = explode("\n", trim($value));
            foreach ($str as $line){
                $ln = explode(":", $line);
                var_dump($ln);
            }
        }
        return true;
    }
}