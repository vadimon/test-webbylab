<?php


class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include ($routesPath);
    }


    /**
     * Returns request string
     */
    private function getURL()
    {
        if (!empty($_SERVER['REQUEST_URI'])){
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run()
    {
        //Получить строку запроса
        $url = $this->getURL();


        //Проверить наличие такого запроса в routes.php
        foreach ($this->routes as $urlPattern => $path){
            //порівнюєм $urlPattern и $url
            if (preg_match("~$urlPattern~", $url)) {

                //получаєм внутрішній шлях з зовнішнього згідно правилу
                $internalRoute = preg_replace("`$urlPattern`", $path, $url);

                //визначить контролєр, action, переметри

                $segments = explode('/', $internalRoute);
                $controllerName = array_shift($segments).'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action' . ucfirst(array_shift($segments));
                $parameters = $segments;

                //Подключить файл класса-контролера
                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';


                if (file_exists($controllerFile)){
                    include_once ($controllerFile);
                }

                //Созлать обьект, вызвать метод (т.е. action)
                $controllerObject = new $controllerName;

                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                if ($result != null){
                    break;
                }
            }
        }
    }
}