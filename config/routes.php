<?php
return array(
    'create' => 'site/createTable', //actionCreateTable v SiteController
    'import' => 'site/import', // actionImport v SiteController
    'search' => 'site/search', // actionSearch v SiteController
    'load' => 'site/load', // actionLoad v SiteController
    'delete/([0-9]+)' => 'site/delete/$1', // actionDelete v SiteController
    'add_film' => 'site/add', //actionAdd v SiteController
    'show/([0-9]+)' => 'site/show/$1', //actionShow v SiteController
    'index' => 'site/index', //actionIndex v SiteController
    '' => 'site/index', //actionIndex v SiteController

);
